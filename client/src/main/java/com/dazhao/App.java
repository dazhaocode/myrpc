package com.dazhao;

import com.dazhao.server.entity.User;
import com.dazhao.server.rpc.RpcProxy;
import com.dazhao.server.rpc.RpcService;
import com.dazhao.server.service.UserService;

import java.util.List;

/**
 * @author dazhao
 * @since 2020/11/28 5:39 下午
 */
public class App {
    public static void main(String[] args) {
        User user = new User();
        user.setUsername("aaa");
        user.setPassword("bbb");
        UserService service = (UserService) RpcService.service(UserService.class,"UserService");
        List<User> users = service.queryUserByParam(user);
        System.out.println(users.toString());
    }
}
