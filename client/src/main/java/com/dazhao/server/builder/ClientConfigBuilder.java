package com.dazhao.server.builder;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.File;
import java.util.*;

/**
 * 解析配置文件
 * @Author dazhao
 * @Date 2020/11/14 12:27 下午
 */
public class ClientConfigBuilder {
    private static  Map<String,Map<String,String>> serverMap =new HashMap<>();
    private static  List<String> registryHostList = new ArrayList<>();
    private static String serverHost;
    private static Integer serverPort;
    static {
        try {
            //解析xml
            buildXml();
        } catch (Exception e) {
           e.printStackTrace();
        }
    }
    /**
     * 解析rpc.xml
     */
    public static void buildXml() throws Exception {
        String xmlPath = Objects.requireNonNull(ClientConfigBuilder.class.getClassLoader()
                .getResource("config/rpc.xml")).getPath();
        SAXReader saxReader=new SAXReader();
        Document document = saxReader.read(new File(xmlPath));
        Element rootElement = document.getRootElement();
        List<Element> beans = rootElement.elements("bean");
        for (Element bean : beans) {

            if ("registry".equals(bean.attributeValue("name"))) {
                //注册中心的信息
                List<Element> properties = bean.elements("properties");
                for (Element property : properties) {
                    if ("host".equals(property.attributeValue("name"))) {
                        registryHostList.add(property.getTextTrim());
                    }
                }
            }else if ("server".equals(bean.attributeValue("name"))){
                //服务信息识别
                 serverHost = bean.attributeValue("serverHost");
                 serverPort = Integer.valueOf(bean.attributeValue("serverPort"));
                List<Element> properties = bean.elements("properties");
                for (Element property : properties) {
                    HashMap<String, String> stringObjectHashMap = new HashMap<>();
                    stringObjectHashMap.put("serverInterface", property.attributeValue("interface"));
                    stringObjectHashMap.put("serverImpl", property.attributeValue("impl"));
                    serverMap.put(property.attributeValue("name"),stringObjectHashMap);
                }
            }
        }
    }

    public static String getServerHost() {
        return serverHost;
    }

    public static Integer getServerPort() {
        return serverPort;
    }

    public static Map<String,String> getServerMap(String serverName) {
        return serverMap.get(serverName);
    }

    public static List<String> getRegistryHostList() {
        return registryHostList;
    }
}
