package com.dazhao.server.rpc;

import com.dazhao.server.builder.ClientConfigBuilder;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.net.Socket;
import java.util.Arrays;
import java.util.Map;

/**
 * @author dazhao
 * @since 2020/11/28 5:45 下午
 */
@SuppressWarnings("all")
public class RpcProxy implements InvocationHandler {

    private String serviceName;
    private String registryHost;
    private Integer registryPort;
    private Map<String, String> serverMap;
    private String serverHost;
    private String taskNumber;

    public RpcProxy(String serviceName) {
        this.serviceName = serviceName;
        this.registryHost = ClientConfigBuilder.getRegistryHostList().get(0).split(":")[0];
        this.registryPort = Integer.valueOf(ClientConfigBuilder.getRegistryHostList().get(0).split(":")[1]);
        this.serverMap = ClientConfigBuilder.getServerMap(this.serviceName);
    }

    private void getServer(Method method) throws JsonProcessingException {
        String methodName = method.getName();
        String methodParamsTypes = Arrays.toString(method.getParameterTypes());
        serverMap.put("serverMethodName", methodName);
        serverMap.put("serverMethodParams", methodParamsTypes);
        ObjectMapper objectMapper = new ObjectMapper();
        String serverJson = objectMapper.writeValueAsString(serverMap);
        try {
            Socket socket = new Socket(this.registryHost, this.registryPort);
            ObjectOutputStream outputStream = new ObjectOutputStream(socket.getOutputStream());
            ObjectInputStream inputStream = new ObjectInputStream(socket.getInputStream());
            outputStream.writeUTF("getServer");
            outputStream.writeUTF(serverJson);
            //关闭
            outputStream.flush();
            socket.shutdownOutput();
            if (!"200".equals(inputStream.readUTF())) {
                throw new Exception("服务获取异常！");
            }
            //服务地址 注册中心返回的服务地址
            serverHost = inputStream.readUTF();
            //任务编号 注册中心返回的服务的任务编号
            taskNumber = inputStream.readUTF();
            socket.shutdownInput();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public Object sendRequest(Method method,Object[] args){
        Object returnObject = null;
        try {
            String host = serverHost.split(":")[0];
            Integer port = Integer.valueOf(serverHost.split(":")[1]);
            Socket socket = new Socket(host, port);
            ObjectOutputStream outputStream = new ObjectOutputStream(socket.getOutputStream());
            ObjectInputStream inputStream = new ObjectInputStream(socket.getInputStream());
            outputStream.writeUTF("sendRequest");
            outputStream.writeUTF(taskNumber);
            outputStream.writeObject(serverMap);
            outputStream.writeObject(method.getParameterTypes());
            outputStream.writeObject(args);
            //关闭
            outputStream.flush();
            socket.shutdownOutput();
            if (!"200".equals(inputStream.readUTF())) {
                throw new Exception("远程请求异常！");
            }
             returnObject = inputStream.readObject();
            socket.shutdownInput();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return returnObject;
    }
    //提交ack
    private void ack(String taskNumber,Integer ack) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Socket socket = new Socket(registryHost, registryPort);
                    ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream());
                    out.writeUTF("taskACK");
                    out.writeUTF(taskNumber);
                    out.writeInt(ack);
                    out.flush();
                    socket.shutdownOutput();
                    ObjectInputStream in = new ObjectInputStream(socket.getInputStream());
                    String result = in.readUTF();
                    socket.shutdownInput();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        this.getServer(method);
        Object resultObject = this.sendRequest(method, args);
        this.ack(taskNumber,4);
        return resultObject;
    }
}
