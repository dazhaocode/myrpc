package com.dazhao.server.rpc;

import java.lang.reflect.Proxy;

/**
 * @author dazhao
 * @since 2020/11/28 5:47 下午
 */
public class RpcService {

    public  static  Object service(Class<?> interfaceClazz,String serviceName){
       //创建代理
        return Proxy.newProxyInstance(interfaceClazz.getClassLoader(), new Class[]{interfaceClazz}, new RpcProxy(serviceName));
    }
}
