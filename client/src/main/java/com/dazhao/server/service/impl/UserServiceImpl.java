package com.dazhao.server.service.impl;


import com.dazhao.server.entity.User;
import com.dazhao.server.service.UserService;

import java.util.List;

/**
 * @author dazhao
 * @since 2020/11/25 9:02 下午
 */
public class UserServiceImpl implements UserService {


    @Override
    public List<User> queryUserByParam(User user) {
        return null;
    }


    @Override
    public int addUserByParam(User user) {
        return 0;
    }


    @Override
    public int updateUserByParam(User user) {
        return 0;
    }


    @Override
    public int deleteUserByParam(User user) {
        return 0;
    }
}
