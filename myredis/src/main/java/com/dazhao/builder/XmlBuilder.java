package com.dazhao.builder;

import com.dazhao.entity.ElementEntity;
import com.dazhao.exception.GlobalException;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;

import java.io.File;
import java.io.FileOutputStream;
import java.util.*;

/**
 * 解析数据文档
 * @Author dazhao
 * @Date 2020/11/14 1:18 下午
 */
public class  XmlBuilder {
    //设置为单例
    private static XmlBuilder xmlBuilder=null;
    private Document document=null;
    private Element root=null;
    private String path;
    private List<Element> elementList=new ArrayList<Element>();
    private XmlBuilder(){
         path = Objects.requireNonNull(this.getClass().getClassLoader().getResource("database/database.xml")).getPath();
    }
    public synchronized static XmlBuilder newInstance(){
        if (xmlBuilder==null) {
            xmlBuilder=new XmlBuilder();
        }
        return xmlBuilder;
    }

    public synchronized void getConnect(){
        //读取文档
        try {
            SAXReader saxReader = new SAXReader();
            document=saxReader.read(new File(path));
            //读取root
            root = document.getRootElement();
            elementList = root.elements("data");
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }
    /**
     * 获取序列
     * @return
     */
    public synchronized Integer getSequence() {
        Element sequence = root.element("sequence");
        String value = sequence.getTextTrim();
        return Integer.valueOf(value);
    }
    /**
     * 设置序列
     * @return
     */
    public synchronized void setSequence(Integer sequenceValue) {
        Element sequence = root.element("sequence");
        sequence.setText(String.valueOf(sequenceValue));
    }



    /**
     * 添加一个标签
     * @param id
     * @param key
     * @param value
     */
    public synchronized void  addTag(Integer id,String key,String value){
        Element data = DocumentHelper.createElement("data");
        data.addAttribute("id",String.valueOf(id));
        data.addAttribute("key",key);
        data.addText(value);
        this.elementList.add(data);
    }

    /**
     * 获取全部
     * @return list element
     */
    public synchronized List<ElementEntity> getAll(){
        ArrayList<ElementEntity> elementEntities = new ArrayList<>();
        for (Element element : elementList) {
                elementEntities.add(new ElementEntity().setId(Integer.valueOf(element.attributeValue("id"))).setKey(element.attributeValue("key")).setValue(element.getTextTrim()));
            }
        return elementEntities ;
    }
    /**
     * 根据key获取一批标签
     * @param key
     * @return list element
     */
    public synchronized List<ElementEntity> getTag(String key){
        ArrayList<ElementEntity> elementEntities = new ArrayList<>();
        for (Element element : elementList) {
            if (element.attribute("key").getValue().equals(key)) {
                elementEntities.add(new ElementEntity().setId(Integer.valueOf(element.attributeValue("id"))).setKey(key).setValue(element.getTextTrim()));
            }
        }
        return elementEntities ;
    }

    /**
     * 根据id 获取一个标签
     * @param id
     * @return
     */
    public synchronized ElementEntity getTag(Integer id){
        for (Element element : elementList) {
            if (Integer.valueOf(element.attributeValue("id")).equals(id)) {
              return  new ElementEntity().setId(id).setKey(element.attributeValue("key")).setValue(element.getTextTrim());
            }
        }
        return null;
    }
    /**
     * 将已经修改的写入xml
     * @return
     */
    public synchronized void close() throws GlobalException{
        //格式化
        try {
            OutputFormat outputFormat=OutputFormat.createPrettyPrint();
            outputFormat.setEncoding("UTF-8");
            XMLWriter xmlWriter = new XMLWriter(new FileOutputStream(new File(path)),outputFormat);
            xmlWriter.write(document);
        } catch (Exception e) {
            //抛出全局异常
            throw new GlobalException();
        }
    }

    /**
     * 根据id或者key修改
     * @param id
     * @param key
     * @param value
     */
    public  synchronized  void update(Integer id,String key,String value){
        //id 为null 批量修改key
        if (id==null) {
            for (Element element : elementList) {
                if (element.attributeValue("key").equals(key)) {
                    element.setText(value);
                }
            }
            return;
        }
        for (Element element : elementList) {
            if (element.attributeValue("id").equals(String.valueOf(id))) {
                element.addAttribute("key",key);
                element.setText(value);
            }
        }
    }
    /**
     * 根据id或者key删除
     * @param id
     * @param key
     */
    public  synchronized  void remove(Integer id,String key){
        //id 为null 批量删除key相同的 这里找出相同的 放到待删除的集合
        ArrayList<Element> elseElement = new ArrayList<>();
        if (id==null) {
            for (Element element : elementList) {
                if (element.attributeValue("key").equals(key)) {
                    elseElement.add(element);
                }
            }
        }else {
            for (Element element : elementList) {
                if (element.attributeValue("id").equals(String.valueOf(id))) {
                    elseElement.add(element);
                }
            }
        }
        for (Element element : elseElement) {
            this.elementList.remove(element);
        }
    }
}
