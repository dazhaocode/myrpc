package com.dazhao.entity;

/**
 * @Author dazhao
 * @Date 2020/11/14 1:59 下午
 */
public class ElementEntity {
    private Integer id;
    private String key;
    private String value;

    public Integer getId() {
        return id;
    }

    public ElementEntity setId(Integer id) {
        this.id = id;
        return this;
    }

    public String getKey() {
        return key;
    }

    public ElementEntity setKey(String key) {
        this.key = key;
        return this;
    }

    public String getValue() {
        return value;
    }

    public ElementEntity setValue(String value) {
        this.value = value;
        return this;
    }

    @Override
    public String toString() {
        return "ElementEntity{" +
                "id=" + id +
                ", key='" + key + '\'' +
                ", value='" + value + '\'' +
                '}';
    }
}
