package com.dazhao.main;

import com.dazhao.tcp.TCPServer;

/**
 * 主入口
 * @Author dazhao
 * @Date 2020/11/14 1:21 下午
 */
public class Main {
    public static void main(String[] args) {
        //启动服务
        TCPServer.newInstance().initAndStart();
    }
}
