package com.dazhao.server;

import com.dazhao.builder.XmlBuilder;
import com.dazhao.entity.ElementEntity;
import java.util.List;

/**
 * 负责数据服务的处理
 * @Author dazhao
 * @Date 2020/11/14 1:22 下午
 */
public class DBServer {
    //静态代理
    private XmlBuilder xmlBuilder=null;

    public DBServer(){
        xmlBuilder=XmlBuilder.newInstance();
    }
    public void getConnect(){
        this.xmlBuilder.getConnect();
    }
    /**
     * 获取序列
     * @return
     */
    private Integer getSequence() {
       return this.xmlBuilder.getSequence();
    }
    /**
     * 设置序列
     * @return
     */
    private  void setSequence(Integer sequenceValue) {
      this.xmlBuilder.setSequence(sequenceValue);
    }

    /**
     * 添加一个标签
     * @param key
     * @param value
     */
    public synchronized void  addTag(String key,String value){
        Integer id = this.getSequence()+1;
        //现在的基础上加1
        this.xmlBuilder.addTag(id,key,value);
        this.setSequence(id);
    }

    /**
     * 获取全部
     * @return tags
     */
    public List<ElementEntity>getAll(){
        return this.xmlBuilder.getAll();
    }
    /**
     * 根据key获取一批标签
     * @param key
     * @return list element
     */
    public  List<ElementEntity> getTag(String key){
        return this.xmlBuilder.getTag(key);
    }

    /**
     * 根据id 获取一个标签
     * @param id
     * @return
     */
    public synchronized ElementEntity getTag(Integer id){
        return this.xmlBuilder.getTag(id);
    }

    /**
     * 将已经修改的写入xml
     * @return
     */
    public void close(){
        this.xmlBuilder.close();
    }

    /**
     * 根据id修改
     * @param id
     * @param key
     * @param value
     */
    public  synchronized void updateById(Integer id,String key,String value){
        this.xmlBuilder.update(id,key,value);
    }

    /**
     * 根据key修改
     * @param key
     * @param value
     */
    public  synchronized void updateByKey(String key,String value){
        this.xmlBuilder.update(null,key,value);
    }

    /**
     * 根据id删除
     */
    public  synchronized void removeById(Integer id){
        this.xmlBuilder.remove(id,null);
    }
    /**
     * 根据key删除
     * @param key
     */
    public  synchronized void removeByKey(String key) {
        this.xmlBuilder.remove(null,key);
    }
}
