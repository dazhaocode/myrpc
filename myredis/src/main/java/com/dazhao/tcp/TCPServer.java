package com.dazhao.tcp;

import com.dazhao.builder.ConfigBuilder;
import com.dazhao.exception.GlobalException;
import java.net.ServerSocket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 通讯服务处理
 * @Author dazhao
 * @Date 2020/11/14 1:22 下午
 */
public class TCPServer {
    //单例
    private static TCPServer tcpServer=null;
    private  Integer port;
    private  Integer maxActivity;
    private TCPServer(){
        this.port= ConfigBuilder.getPort();
        this.maxActivity=ConfigBuilder.getMaxActivity();
    }
    public synchronized  static TCPServer newInstance() {
        if (tcpServer==null) {
            tcpServer=new TCPServer();
        }
        return tcpServer;
    }

    /**
     * 初始化启动服务
     */
    public  void initAndStart(){
        try {
            System.out.println("启动服务中....");
            //不关闭服务 一直提供服务
            ServerSocket serverSocket=new ServerSocket(port);
            //根据maxActivity 创建线程池
            ExecutorService executorService= Executors.newFixedThreadPool(this.maxActivity);
            //创建TcpServlet 所有线程公用一个此TcpServlet
            TcpServlet tcpServlet = new TcpServlet();
            System.out.println("服务启动成功！");
            while (true){
                executorService.execute(new TcpThread(
                        serverSocket.accept(),
                            ConfigBuilder.getUsername(),
                                ConfigBuilder.getPassword(),
                                    tcpServlet
                        ));
            }
        } catch (Exception e) {
            throw new GlobalException();
        }
    }
}
