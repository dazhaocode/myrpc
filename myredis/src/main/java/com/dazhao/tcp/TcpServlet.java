package com.dazhao.tcp;

import com.dazhao.entity.ElementEntity;
import com.dazhao.server.DBServer;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * @Author dazhao
 * @Date 2020/11/17 10:47 下午
 */
public class TcpServlet {
    /**
     * false 非公平锁可能存在饥饿限制 true 公平锁
     * 读写锁
     */
    private ReadWriteLock readWriteLock=new ReentrantReadWriteLock(false);
    private Lock readLock=null;
    private Lock writeLock=null;
    public TcpServlet(){
        this.readLock=readWriteLock.readLock();
        this.writeLock=readWriteLock.writeLock();
    }
    public void  add(ObjectInputStream inputStream, ObjectOutputStream outputStream, Socket socket) throws IOException {
        this.writeLock.lock();
        System.out.println("add");
        String key = inputStream.readUTF();
        String value = inputStream.readUTF();
        //终止输入
        socket.shutdownInput();
        DBServer dbServer=new DBServer();
        dbServer.getConnect();
        dbServer.addTag(key,value);
        dbServer.close();
        //返回信息
        outputStream.writeUTF("200");
        this.writeLock.unlock();

    }
    public void  getAll(ObjectInputStream inputStream, ObjectOutputStream outputStream, Socket socket) throws IOException {
        this.readLock.lock();
        socket.shutdownInput();
        DBServer dbServer=new DBServer();
        dbServer.getConnect();
        List<ElementEntity> all = dbServer.getAll();
        dbServer.close();
        ObjectMapper objectMapper=new ObjectMapper();
        String responseJson = objectMapper.writeValueAsString(all);
        //返回信息
        outputStream.writeUTF("200");
        outputStream.writeUTF(responseJson);
        this.readLock.unlock();
    }
    public void  getById(ObjectInputStream inputStream, ObjectOutputStream outputStream, Socket socket) throws IOException {
        this.readLock.lock();
        Integer id = inputStream.readInt();
        socket.shutdownInput();
        DBServer dbServer=new DBServer();
        dbServer.getConnect();
        ElementEntity tag = dbServer.getTag(id);
        dbServer.close();
        ObjectMapper objectMapper=new ObjectMapper();
        String responseJson = objectMapper.writeValueAsString(Collections.singletonList(tag));
        //返回信息
        outputStream.writeUTF("200");
        outputStream.writeUTF(responseJson);
        this.readLock.unlock();
    }
    public void  getByKey(ObjectInputStream inputStream, ObjectOutputStream outputStream, Socket socket) throws IOException {
        this.readLock.lock();
        String key = inputStream.readUTF();
        socket.shutdownInput();
        DBServer dbServer=new DBServer();
        dbServer.getConnect();
        List<ElementEntity> tag = dbServer.getTag(key);
        dbServer.close();
        ObjectMapper objectMapper=new ObjectMapper();
        String responseJson = objectMapper.writeValueAsString(tag);
        //返回信息
        outputStream.writeUTF("200");
        outputStream.writeUTF(responseJson);
        this.readLock.unlock();

    }
    public void  removeById(ObjectInputStream inputStream, ObjectOutputStream outputStream, Socket socket) throws IOException {
        this.writeLock.lock();
        Integer id = inputStream.readInt();
        socket.shutdownInput();
        DBServer dbServer=new DBServer();
        dbServer.getConnect();
        dbServer.removeById(id);
        dbServer.close();
        //返回信息
        outputStream.writeUTF("200");
        this.writeLock.unlock();
    }
    public void  removeByKey(ObjectInputStream inputStream, ObjectOutputStream outputStream, Socket socket) throws IOException {
        this.writeLock.lock();
        System.out.println("removebykey");
        String key = inputStream.readUTF();
        System.out.println(key);
        socket.shutdownInput();
        DBServer dbServer=new DBServer();
        dbServer.getConnect();
        dbServer.removeByKey(key);
        dbServer.close();
        //返回信息
        outputStream.writeUTF("200");
        this.writeLock.unlock();
    }
    public void  updateById(ObjectInputStream inputStream, ObjectOutputStream outputStream, Socket socket) throws IOException {
        this.writeLock.lock();
        Integer id = inputStream.readInt();
        String key = inputStream.readUTF();
        String value = inputStream.readUTF();
        //终止输入
        socket.shutdownInput();
        DBServer dbServer=new DBServer();
        dbServer.getConnect();
        dbServer.updateById(id,key,value);
        dbServer.close();
        //返回信息
        outputStream.writeUTF("200");
        this.writeLock.unlock();
    }
    public void  updateByKey(ObjectInputStream inputStream, ObjectOutputStream outputStream, Socket socket) throws IOException {
        this.writeLock.lock();
        String key = inputStream.readUTF();
        String value = inputStream.readUTF();
        //终止输入
        socket.shutdownInput();
        DBServer dbServer=new DBServer();
        dbServer.getConnect();
        dbServer.updateByKey(key,value);
        dbServer.close();
        //返回信息
        outputStream.writeUTF("200");
        this.writeLock.unlock();
    }
}
