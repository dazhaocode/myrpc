package com.dazhao.tcp;

import com.dazhao.exception.GlobalException;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

/**
 * @Author dazhao
 * @Date 2020/11/17 10:19 下午
 */
public class TcpThread implements Runnable {
    private final Socket socket;
    private final String username;
    private final String password;
    private final TcpServlet tcpServlet;
    public TcpThread(final Socket socket,
                        final String username,
                            final String password,
                                final TcpServlet tcpServlet){
        this.socket=socket;
        this.username=username;
        this.password=password;
        this.tcpServlet=tcpServlet;
    }
    @Override
    public void run() {
        //设计协议 用户 密码 行为 参数
        //返回结果说明 200 OK 400 notfound 500 501 account incorrect 502 params incorrect
        ObjectInputStream inputStream=null;
        ObjectOutputStream outputStream=null;
        try {
             inputStream = new ObjectInputStream(this.socket.getInputStream());
             outputStream = new ObjectOutputStream(this.socket.getOutputStream());
            //先读username
            String username = inputStream.readUTF();
            String password = inputStream.readUTF();
            if (this.username.equals(username)&&this.password.equals(password)) {
                //读取操作类型
                String operation = inputStream.readUTF();
                switch (operation) {
                    case "add":
                        tcpServlet.add(inputStream,outputStream,this.socket);
                        break;
                    case "getAll":
                        tcpServlet.getAll(inputStream, outputStream,this.socket);
                        break;
                    case "getById":
                        tcpServlet.getById(inputStream,outputStream,this.socket);
                        break;
                    case "getByKey":
                        tcpServlet.getByKey(inputStream,outputStream,this.socket);
                        break;
                    case "updateByKey":
                        tcpServlet.updateByKey(inputStream, outputStream,this.socket);
                        break;
                    case "updateById":
                        tcpServlet.updateById(inputStream,outputStream,this.socket);
                        break;
                    case "removeById":
                        tcpServlet.removeById(inputStream,outputStream,this.socket);
                        break;
                    case "removeByKey":
                        tcpServlet.removeByKey(inputStream,outputStream,this.socket);
                        break;

                }
            }else{
                outputStream.writeUTF("501");
            }
        } catch (IOException e) {
            if (outputStream!=null) {
                try {
                    outputStream.writeUTF("500");
                    //写出异常
                    outputStream.writeObject(e);
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                }
            }

        }finally {
//            if (inputStream!=null) {
//                try {
//                    inputStream.close();
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
            if (outputStream!=null) {
                try {
                    outputStream.flush();
                    //已经shutdownOutput 可以不关闭
//                    this.socket.shutdownOutput();
//                    outputStream.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

    }
}
