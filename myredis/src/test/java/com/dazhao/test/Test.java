package com.dazhao.test;

import com.dazhao.server.DBServer;
import com.dazhao.tcp.TCPServer;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

/**
 * @Author dazhao
 * @Date 2020/11/14 3:09 下午
 */
public class Test {
    public static void main(String[] args) throws IOException {
        DBServer dbServer=new DBServer();
        dbServer.getConnect();
        dbServer.removeByKey("127.0.0.1:9997");
        dbServer.close();

//        Socket socket=new Socket("localhost",9996);
//        ObjectOutputStream outputStream = new ObjectOutputStream(socket.getOutputStream());
//        outputStream.writeUTF("root");
//        outputStream.writeUTF("root");
//        outputStream.writeUTF("getByKey");
//        outputStream.writeUTF("socketTest");
//        outputStream.flush();
//        socket.shutdownOutput();
//        //读取
//        ObjectInputStream inputStream = new ObjectInputStream(socket.getInputStream());
//        System.out.println(inputStream.readUTF());
//        System.out.println(inputStream.readUTF());
//        socket.shutdownInput();
//        inputStream.close();
//        outputStream.close();
    }
}
