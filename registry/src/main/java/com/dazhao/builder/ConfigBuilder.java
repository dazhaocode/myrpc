package com.dazhao.builder;
import java.io.File;
import java.io.FileInputStream;
import java.util.Objects;
import java.util.Properties;

/**
 * 解析配置文件
 * @Author dazhao
 * @Date 2020/11/14 12:27 下午
 */
public class ConfigBuilder {
    private static String path;
    private static Integer port;
    private static Integer maxActivity;
    static {
        try {
            path = Objects.requireNonNull(ConfigBuilder.class.getClassLoader()
                    .getResource("config/server.properties")).getPath();
            System.out.println(path);
            Properties properties = new Properties();
            properties.load(new FileInputStream(new File(path)));
            port=Integer.valueOf(properties.getProperty("port"));
            maxActivity=Integer.valueOf(properties.getProperty("maxActivity"));
        } catch (Exception e) {
           e.printStackTrace();
        }
    }
    public static Integer getMaxActivity() {
        return maxActivity;
    }

    public static Integer getPort() {
        return port;
    }

}
