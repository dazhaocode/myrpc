package com.dazhao.database.dao;

import com.dazhao.database.entity.ResultEntity;

import java.util.List;

/**
 * @Author dazhao
 * @Date 2020/11/19 9:00 下午
 */
public interface BasicDao {
    List<ResultEntity> queryEntity(Integer id,String key);
    void updateEntity(Integer id,String key,String value);
    void deleteEntity(Integer id,String key);
    void insertEntity(String key,String value);
}
