package com.dazhao.database.dao.impl;

import com.dazhao.database.dao.BasicDao;
import com.dazhao.database.driver.Connection;
import com.dazhao.database.driver.DriverManager;
import com.dazhao.database.driver.ResultSet;
import com.dazhao.database.driver.Statement;
import com.dazhao.database.entity.ResultEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * @author dazhao
 * @since 2020/11/22 12:14 上午
 */
public class BasicDaoImpl implements BasicDao {
    private final String HOST="127.0.0.1:9996";
    private final String USERNAME="root";
    private final String PASSWORD="root";
    @Override
    public List<ResultEntity> queryEntity(Integer id, String key) {
        Connection connection = DriverManager.getConnection(HOST, USERNAME, PASSWORD);
        Statement statement = connection.statement();
        StringBuffer sql=new StringBuffer();
        if (id==null&&key==null) {
            sql.append("get");
        } else if(id==null&&key!=null){
            sql.append("getByKey");
            sql.append(",");
            sql.append(key);
        }else if (id!=null&&key==null){
            sql.append("getById");
            sql.append(",");
            sql.append(id);
        }
        ResultSet resultSet = statement.executeQuery(sql.toString());
        List<ResultEntity> resultEntities = new ArrayList<>();
        while (resultSet.next()){
            ResultEntity resultEntity = new ResultEntity()
                    .setId(resultSet.getInt("id"))
                    .setKey(resultSet.getString("key"))
                    .setValue(resultSet.getString("value"));
            resultEntities.add(resultEntity);
        }
        return resultEntities;
    }

    @Override
    public void updateEntity(Integer id, String key, String value) {
        Connection connection = DriverManager.getConnection(HOST, USERNAME, PASSWORD);
        Statement statement = connection.statement();
        StringBuffer sql=new StringBuffer();
        if (id!=null) {
            sql.append("updateById");
            sql.append(",");
            sql.append(id);
            sql.append(",");
            sql.append(key);
            sql.append(",");
            sql.append(value);
        } else if(id==null) {
            sql.append("updateByKey");
            sql.append(",");
            sql.append(key);
            sql.append(",");
            sql.append(value);
        }
        statement.executeUpdate(sql.toString());
    }

    @Override
    public void deleteEntity(Integer id, String key) {
        Connection connection = DriverManager.getConnection(HOST, USERNAME, PASSWORD);
        Statement statement = connection.statement();
        StringBuffer sql=new StringBuffer();
        if (id!=null) {
            sql.append("removeById");
            sql.append(",");
            sql.append(id);
        } else if(id==null) {
            sql.append("removeByKey");
            sql.append(",");
            sql.append(key);
        }
        statement.executeUpdate(sql.toString());
    }

    @Override
    public void insertEntity(String key, String value) {
        Connection connection = DriverManager.getConnection(HOST, USERNAME, PASSWORD);
        Statement statement = connection.statement();
        StringBuffer sql=new StringBuffer();
        sql.append("add");
        sql.append(",");
        sql.append(key);
        sql.append(",");
        sql.append(value);
        statement.executeUpdate(sql.toString());
    }
}
