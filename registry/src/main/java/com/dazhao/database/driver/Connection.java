package com.dazhao.database.driver;

import java.io.*;
import java.net.Socket;

/**
 * @Author dazhao
 * @Date 2020/11/19 9:04 下午
 */
public class Connection {
    private String url;
    private String username;
    private String password;
    public Connection(){
    }

    private ObjectOutputStream outputStream;
    private ObjectInputStream inputStream;
    public Connection(String url, String username, String password) {
        this.url = url;
        this.username = username;
        this.password = password;
    }

    /**
     * 封装socket
     * @return
     * @throws IOException
     */
    private Socket socketInfo() throws IOException {
        String ip = url.split(":")[0];
        String port=url.split(":")[1];
        Socket socket = new Socket(ip, Integer.parseInt(port));
        outputStream = new ObjectOutputStream(socket.getOutputStream());
        inputStream = new ObjectInputStream(socket.getInputStream());
        outputStream.writeUTF(username);
        outputStream.writeUTF(password);
        return socket;
    }
    public Statement statement(){
        try {
            //需要先执行socketInfo
            Socket socket = socketInfo();
            return new Statement(outputStream,inputStream, socket);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
