package com.dazhao.database.driver;

/**
 * @Author dazhao
 * @Date 2020/11/19 9:00 下午
 */
public class DriverManager {

    /**
     * 获取连接
     * @param url
     * @param username
     * @param password
     */
    public static Connection getConnection(String url,String username,String password){
        return new Connection(url,username,password);
    }
}
