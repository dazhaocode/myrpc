package com.dazhao.database.driver;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author dazhao
 * @date 2020/11/19 10:49 下午
 */
public class ResultSet {
   private List<Map<String,Object>> list=null;
   private Integer size=0;
    public void build(String json) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            list = objectMapper.readValue(json, List.class);
            this.size= list.size();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }
    private Integer index=-1;
    private Map<String,Object> result=new HashMap<>();
    public boolean next(){
         if ((index+1)>=size) {
            return false;
        }else {
            index++;
            result=this.list.get(index);
            return true;
        }
    }
    public Integer getInt(String  key){
        return (Integer) this.result.get(key);
    }
    public String getString(String  key){
        return (String) this.result.get(key);
    }
}
