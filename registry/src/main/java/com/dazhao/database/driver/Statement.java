package com.dazhao.database.driver;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;

/**
 * @author dazhao
 * @date 2020/11/19 9:25 下午
 */
@SuppressWarnings("all")
public class Statement {
    private ObjectOutputStream objectOutputStream=null;
    private ObjectInputStream objectInputStream=null;
    private Socket socket=null;

    public Statement(ObjectOutputStream objectOutputStream, ObjectInputStream objectInputStream, Socket socket) {
        this.objectOutputStream = objectOutputStream;
        this.objectInputStream = objectInputStream;
        this.socket = socket;
    }

    /**
     * get
     * getById,id
     * getByKey,key
     * @param sql
     */
    public ResultSet executeQuery(String sql){
        final String[] split = sql.split(",");
        final String operation = split[0];
        ResultSet resultSet = new ResultSet();
        try {
            switch (operation.toLowerCase()) {
                case "get":
                    objectOutputStream.writeUTF("getAll");
                    break;
                case "getbyid":
                    String id = split[1];
                    objectOutputStream.writeUTF("getById");
                    objectOutputStream.writeInt(Integer.parseInt(id));
                    break;
                case "getbykey":
                    String key = split[1];
                    objectOutputStream.writeUTF("getByKey");
                    objectOutputStream.writeUTF(key);
                    break;
                default:
                    throw new Exception("操作不支持..");
            }
            objectOutputStream.flush();
            this.socket.shutdownOutput();
            String result = objectInputStream.readUTF();
            if (!"200".equals(result)) {
                throw new Exception(((Exception) objectInputStream.readObject()));
            }
            resultSet.build(objectInputStream.readUTF());
        }catch (Exception e){
            e.printStackTrace();
        }
        return resultSet;
    }
    /**
     * add,key,value
     * removeById,id
     * removeByKey,key
     * updateByKey,key,value
     * updateById,id,key,value
     * @param sql
     */
    public void executeUpdate(String sql){
        String[] split = sql.split(",");
        String operation = split[0];
       try {
           switch (operation.toLowerCase()) {
               case "add":
                   String key = split[1];
                   String value = split[2];
                   objectOutputStream.writeUTF("add");
                   objectOutputStream.writeUTF(key);
                   objectOutputStream.writeUTF(value);
                   break;
               case "removebyid":
                   String id = split[1];
                   objectOutputStream.writeUTF("removeById");
                   objectOutputStream.writeInt(Integer.parseInt(id));
                   break;
               case "removebykey":
                   String removeKey = split[1];
                   objectOutputStream.writeUTF("removeByKey");
                   objectOutputStream.writeUTF(removeKey);
                   break;
               case "updatebyid":
                   String updateId = split[1];
                   String updateKey = split[2];
                   String updateValue = split[3];
                   objectOutputStream.writeUTF("updateById");
                   objectOutputStream.writeInt(Integer.parseInt(updateId));
                   objectOutputStream.writeUTF(updateKey);
                   objectOutputStream.writeUTF(updateValue);
                   break;
               case "updatebykey":
                   String updateKey2 = split[1];
                   String updateValue2 = split[2];
                   objectOutputStream.writeUTF("updateByKey");
                   objectOutputStream.writeUTF(updateKey2);
                   objectOutputStream.writeUTF(updateValue2);
                   break;
               default:
                   throw new Exception("操作不支持..");
           }
           objectOutputStream.flush();
           this.socket.shutdownOutput();
           if (!"200".equals(objectInputStream.readUTF())) {
               throw new Exception(((Exception) objectInputStream.readObject()));
           }
       }catch (Exception e){
           e.printStackTrace();
       }
    }
}
