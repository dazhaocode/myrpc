package com.dazhao.database.entity;

/**
 * @author dazhao
 * @since 2020/11/22 12:11 上午
 */
public class ResultEntity {
    private Integer id;
    private String  key;
    private String  value;

    public Integer getId() {
        return id;
    }

    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

    public ResultEntity setId(Integer id) {
        this.id = id;
        return this;
    }

    public ResultEntity setKey(String key) {
        this.key = key;
        return this;
    }

    public ResultEntity setValue(String value) {
        this.value = value;
        return this;
    }
}
