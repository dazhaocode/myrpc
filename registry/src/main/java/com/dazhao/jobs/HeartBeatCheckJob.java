package com.dazhao.jobs;

import com.dazhao.database.dao.BasicDao;
import com.dazhao.database.dao.impl.BasicDaoImpl;
import com.dazhao.database.entity.ResultEntity;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author dazhao
 * @since 2020/11/24 10:00 下午
 * 心跳检测定时任务 timer实现
 */
public class HeartBeatCheckJob {
    /**
     * 定时任务
     */
    public static void  info(){
        Timer timer=new Timer();
        timer.schedule(new TimerTask() {
            private BasicDao basicDao=new BasicDaoImpl();
            @Override
            public void run() {
                List<ResultEntity> resultEntities = basicDao.queryEntity(null, null);
                Long now = System.currentTimeMillis();
                for (ResultEntity resultEntity : resultEntities) {
                    if (resultEntity.getKey().endsWith(":heartBeat")) {
                        Long heartTime = Long.valueOf(resultEntity.getValue());
                        if (now-heartTime>3000) {
                            //服务心跳超时
                            String host = resultEntity.getKey().replaceAll(":heartBeat", "");
                            //注销心跳
                            basicDao.deleteEntity(null,resultEntity.getKey());
                            //注销服务
                            basicDao.deleteEntity(null,host+":server");
                            //注销路由
                            basicDao.deleteEntity(null,host);
                            System.out.println(host+"已经注销");
                        }
                    }
                }
            }
        },0,3000);//延迟0ms 即立即启动 每隔3s执行一次
    }

    public static void main(String[] args) {
//        Pattern pattern=Pattern.compile("\\d+\\.\\d+\\.\\d+\\.\\d+:\\d+:\\w+");
//        Matcher matcher = pattern.matcher("127.0.0.1:8080:heartBeat");
//        System.out.println(matcher.matches());
        System.out.println("127.0.0.1:8080:heartBeat".replaceAll("(:\\w+)$", ""));
    }
}
