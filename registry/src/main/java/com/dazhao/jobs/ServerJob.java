package com.dazhao.jobs;

import com.dazhao.database.dao.BasicDao;
import com.dazhao.database.dao.impl.BasicDaoImpl;
import com.dazhao.database.entity.ResultEntity;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * @author dazhao
 * @since 2020/11/28 8:52 下午
 */
public class ServerJob {
    /**
     * 定时检测任务状态
     */
    public static void  info(){
        Timer timer=new Timer();
        timer.schedule(new TimerTask() {
            private BasicDao basicDao=new BasicDaoImpl();
            @Override
            public void run() {
                List<ResultEntity> resultEntities = basicDao.queryEntity(null, null);
                Long now = System.currentTimeMillis();
                for (ResultEntity resultEntity : resultEntities) {
                    if(resultEntity.getKey().endsWith(":seq")) {
                        Integer seq = Integer.valueOf(resultEntity.getValue());
                        if(seq == 4) {//任务完成清理掉
                            String jobNumber = resultEntity.getKey().replaceAll(":seq", "");
                            String host = basicDao.queryEntity(null, jobNumber+":taskHost").get(0).getValue();
                            //减路由
                            List<ResultEntity> res = basicDao.queryEntity(null, host);
                            if(res.size()>0) {
                                Integer luyou = Integer.valueOf(res.get(0).getValue());
                                luyou = Math.max((luyou - 1), 0);
                                basicDao.updateEntity(null, host, String.valueOf(luyou));//修改路由
                                basicDao.deleteEntity(null, jobNumber+":taskTime");
                                basicDao.deleteEntity(null, jobNumber+":seq");
                                basicDao.deleteEntity(null, jobNumber+":taskHost");
                            }
                        }
                    }
                    if (resultEntity.getKey().endsWith(":taskTime")) {
                        Long heartTime = Long.valueOf(resultEntity.getValue());
                        if (now-heartTime>3000) {
                            String taskNumber = resultEntity.getKey().replaceAll(":taskTime", "");
                            String host = this.basicDao.queryEntity(null, taskNumber + ":taskHost").get(0).getValue();
                            //减少路由
                            Integer route = Integer.parseInt(basicDao.queryEntity(null, host).get(0).getValue())-1;
                            route = Math.max(route , 0);
                            //修改路由
                            this.basicDao.updateEntity(null,host, String.valueOf(route));
                            this.basicDao.deleteEntity(null,resultEntity.getKey());
                            this.basicDao.deleteEntity(null,taskNumber+":seq");
                            this.basicDao.deleteEntity(null,taskNumber+":taskHost");
                            System.out.println(host+"已经注销");
                        }
                    }
                }
            }
        },0,3000);//延迟0ms 即立即启动 每隔3s执行一次
    }
}
