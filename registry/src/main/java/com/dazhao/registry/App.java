package com.dazhao.registry;

import com.dazhao.tcp.TCPServer;

/**
 * @Author dazhao
 * @Date 2020/11/19 9:00 下午
 */
public class App {
    public static void main(String[] args) {
        TCPServer.newInstance().initAndStart();
    }
}
