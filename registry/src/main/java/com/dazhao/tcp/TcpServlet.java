package com.dazhao.tcp;

import com.dazhao.database.dao.BasicDao;
import com.dazhao.database.dao.impl.BasicDaoImpl;
import com.dazhao.database.entity.ResultEntity;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.*;

/**
 * @author  dazhao
 * @since 2020/11/17 10:47 下午
 * 注册中心
 */
public class TcpServlet {
    /**
     * 数据操作基类
     */
    private BasicDao basicDao=new BasicDaoImpl();

    /**
     * 心跳检测
     * @param inputStream
     * @param outputStream
     * @param socket
     * @throws IOException
     */
    public void heartBeatCheck(ObjectInputStream inputStream,
                                    ObjectOutputStream outputStream,
                                        Socket socket) throws IOException {
        String ip=inputStream.readUTF();
        Integer port=inputStream.readInt();
        //只需要知道那台服务器发送的心跳
        String key=ip+":"+port+":heartBeat";
        List<ResultEntity> resultEntities = basicDao.queryEntity(null, key);
        if (resultEntities.size()!=0) {
            basicDao.updateEntity(null,key,String.valueOf(new Date().getTime()));
        }else{
            basicDao.insertEntity(key,String.valueOf(new Date().getTime()));
        }
        outputStream.writeUTF("200 --heart checked success！");
    }

    /**
     * 组册服务 和路由
     * 我们需要知道服务提供者的ip和端口
     * 服务提供者的接口信息
     * 如何根据协议内容设计key
     * 路由信息
     * @param inputStream
     * @param outputStream
     * @param socket
     */
    public void  registryServer(ObjectInputStream inputStream,
                                    ObjectOutputStream outputStream,
                                        Socket socket) throws IOException {
        String ip=inputStream.readUTF();
        Integer port=inputStream.readInt();
        String serverJson=inputStream.readUTF();
        ObjectMapper objectMapper=new ObjectMapper();
        List<Map<String,String>> readValue = objectMapper.readValue(serverJson, List.class);
        final String host=ip+":"+port;
        List<ResultEntity> resultEntities = basicDao.queryEntity(null, host);
        if (resultEntities.size()!=0) {
            //先删除 在新增 原本应该清理的服务没有被注销掉 所以先删除一次 以防万一
            basicDao.deleteEntity(null,host);
            basicDao.deleteEntity(null,host+":server");
        }
        //新增注册服务的路由
        basicDao.insertEntity(ip + ":" + port,"0");
        for (Map<String, String> stringObjectMap : readValue) {
            String serviceInterface = stringObjectMap.get("serviceInterface");
            String serviceImpl = stringObjectMap.get("serviceImpl");
            String serviceMethod = stringObjectMap.get("serviceMethod");
            String serviceMethodParams =  stringObjectMap.get("serviceMethodParams");
            String value=ip+":"+port+"//("+serviceInterface
                         +")"+serviceImpl+"."+serviceMethod
                            +":"+ serviceMethodParams;
            //127.0.0.1:8080//com.dazhao.xxx.xxx:[xxx,xxx]
            basicDao.insertEntity(host+":server",value);
        }
        outputStream.writeUTF("200 服务注册成功！");
    }

    public void getServer(ObjectInputStream inputStream,
                            ObjectOutputStream outputStream,
                                Socket socket) throws IOException {

        String serverJson=inputStream.readUTF();
        ObjectMapper objectMapper=new ObjectMapper();
        Map<String,String> serverMap = objectMapper.readValue(serverJson, Map.class);
        String formatServerMapJson = this.formatServerMap(serverMap, null, null);
        List<ResultEntity> resultEntities = this.basicDao.queryEntity(null, null);
        List<String> serverList=new ArrayList<>();
        //找出符合条件的server
        for (ResultEntity resultEntity : resultEntities) {
            if (resultEntity.getKey().endsWith(":server")&&resultEntity.getValue().endsWith(formatServerMapJson)) {
                serverList.add(resultEntity.getKey().replaceAll(":server",""));
            }
        }
        //找出路由最短的server
        Integer minRoute=Integer.MAX_VALUE;
        String host="";
        for (String server : serverList) {
            ResultEntity resultEntity = this.basicDao.queryEntity(null, server).get(0);
            Integer route = Integer.valueOf(resultEntity.getValue());
            String routeHost=resultEntity.getKey();
            //记录路由的host
            if (route<minRoute){
                minRoute=route;
                host=routeHost;
            }
        }
        //生成对应的任务编号
        // TODO: 2020/11/28 这里使用uuid 应当保证分布式下唯一
        String taskNumber = UUID.randomUUID().toString();
        //1.消费者获取任务编号
        //2.生产者确认任务编号
        //3.生产者注销任务编号
        //4.消费者注销任务编号
        this.basicDao.insertEntity(taskNumber+":seq","1");
        this.basicDao.insertEntity(taskNumber+":taskHost",host);
        //每次更新状态 任务时间同步新 如果3s内没有变化 路由-1 任务失效
        this.basicDao.insertEntity(taskNumber+":taskTime", String.valueOf(System.currentTimeMillis()));
        socket.shutdownInput();
        outputStream.writeUTF("200");
        outputStream.writeUTF(host);
        outputStream.writeUTF(taskNumber);
    }

    /**
     * 消费者或者生产者任务确认
     * @param inputStream
     * @param outputStream
     * @param socket
     * @throws IOException
     */
    public void taskAck(ObjectInputStream inputStream,
                          ObjectOutputStream outputStream,
                          Socket socket) throws IOException {
        String taskNumber = inputStream.readUTF();
        Integer ack = inputStream.readInt();
        //如果提交的状态码小于当前序列值，直接丢弃
        //新值(众值)安全，旧值不管
        ResultEntity r = basicDao.queryEntity(null, taskNumber+":seq").get(0);
        Integer seq = Integer.valueOf(r.getValue());
        if(ack>seq) {
            //更新状态码
            basicDao.updateEntity(null, taskNumber+":seq", String.valueOf(ack));
        }
        //小于序列，状态码不变
        //无论状态码是否更新，时间都必须要更新
        basicDao.updateEntity(null, taskNumber+":taskTime", String.valueOf(new Date().getTime()));

        socket.shutdownInput();
        outputStream.writeUTF("200");

    }
    public String formatServerMap(Map<String,String>map,String ip,Integer port) {
        String serviceInterface = map.get("serverInterface");
        String serviceImpl = map.get("serverImpl");
        String serviceMethod = map.get("serverMethodName");
        String serviceMethodParams =  map.get("serverMethodParams");
        String value="";
        if (ip!=null&&port!=null) {
             value=ip+":"+port;
        }
        value+="//("+serviceInterface
                +")"+serviceImpl+"."+serviceMethod
                +":"+ serviceMethodParams;
        return value;
    }
}
