import com.dazhao.database.dao.BasicDao;
import com.dazhao.database.dao.impl.BasicDaoImpl;

/**
 * @author dazhao
 * @date 2020/11/19 9:45 下午
 */
public class Test {
    public static void main(String[] args) {
        //Connection connection = DriverManager.getConnection("127.0.0.1:9996", "root", "root");
        // Statement statement = connection.statement();
        /**
         * add,key,value
         * removeById,id
         * removeByKey,key
         * updateByKey,key,value
         * updateById,id,key,value
         */
        //statement.executeUpdate("add,addKey,addValue2");

        /**
         * get
         * getById,id
         * getByKey,key
         */
//        ResultSet resultSet = statement.executeQuery("get");
//        while (resultSet.next()){
//            System.out.println(resultSet.getInt("id"));
//            System.out.println(resultSet.getString("key"));
//            System.out.println(resultSet.getString("value"));
//        }

        BasicDao basicDao=new BasicDaoImpl();
        basicDao.insertEntity("basicadd","basicadd");
    }
}
