package com.dazhao.server;

import com.dazhao.server.tcp.TCPServer;

/**
 * @author dazhao
 * @since 2020/11/25 8:43 下午
 */
public class ServerMain {
    public static void main(String[] args) {
        //服务生产者
        TCPServer.newInstance().initAndStart();
    }
}
