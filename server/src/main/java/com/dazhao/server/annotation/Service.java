package com.dazhao.server.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author dazhao
 * @since 2020/11/25 9:08 下午
 * 服务注解 需要提供服务的方法就加上该注解
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Service {
    public String value() default "";
}
