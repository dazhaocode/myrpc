package com.dazhao.server.builder;
import com.dazhao.server.annotation.Service;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;


import java.io.File;
import java.io.FileInputStream;
import java.lang.reflect.Method;
import java.util.*;

/**
 * 解析配置文件
 * @Author dazhao
 * @Date 2020/11/14 12:27 下午
 */
public class ConfigBuilder {
    private static String path;
    private static Integer port;
    private static Integer maxActivity;
    public static List<Map<String,String>> list=new ArrayList<>();
    public static List<String> registryHostList = new ArrayList<>();
    static {
        try {
            path = Objects.requireNonNull(ConfigBuilder.class.getClassLoader()
                    .getResource("config/server.properties")).getPath();
            System.out.println(path);
            Properties properties = new Properties();
            properties.load(new FileInputStream(new File(path)));
            port=Integer.valueOf(properties.getProperty("port"));
            maxActivity=Integer.valueOf(properties.getProperty("maxActivity"));
            //解析xml
            buildXml();
        } catch (Exception e) {
           e.printStackTrace();
        }
    }
    public static Integer getMaxActivity() {
        return maxActivity;
    }

    public static Integer getPort() {
        return port;
    }

    /**
     * 解析rpc.xml
     */
    public static void buildXml() throws Exception {
        String xmlPath = Objects.requireNonNull(ConfigBuilder.class.getClassLoader()
                .getResource("config/rpc.xml")).getPath();
        SAXReader saxReader=new SAXReader();
        Document document = saxReader.read(new File(xmlPath));
        Element rootElement = document.getRootElement();
        List<Element> beans = rootElement.elements("bean");
        for (Element bean : beans) {

            if ("registry".equals(bean.attributeValue("name"))) {
                //注册中心的信息
                List<Element> properties = bean.elements("properties");
                for (Element property : properties) {
                    if ("host".equals(property.attributeValue("name"))) {
                        registryHostList.add(property.getTextTrim());
                    }
                }
            }else if ("server".equals(bean.attributeValue("name"))){
                //服务信息识别
                List<Element> properties = bean.elements("properties");
                for (Element property : properties) {
                    String anInterface = property.attributeValue("interface");
                    String impl = property.attributeValue("impl");
                    Class<?> aClass = Class.forName(impl);
                    Method[] methods = aClass.getMethods();
                    for (Method method : methods) {
                        Service annotation = method.getAnnotation(Service.class);
                        if (annotation!=null) {
                            HashMap<String, String> stringObjectHashMap = new HashMap<>();
                            stringObjectHashMap.put("serviceInterface",anInterface);
                            stringObjectHashMap.put("serviceImpl",impl);
                            stringObjectHashMap.put("serviceMethod",method.getName() );
                            stringObjectHashMap.put("serviceMethodParams", Arrays.toString(method.getParameterTypes()));
                            list.add(stringObjectHashMap);
                        }
                    }
                }
            }
        }
    }

    public static List<Map<String, String>> getList() {
        return list;
    }

    public static List<String> getRegistryHostList() {
        return registryHostList;
    }
}
