package com.dazhao.server.entity;

import java.io.Serializable;

/**
 * @author dazhao
 * @since 2020/11/25 8:57 下午
 */
public class User implements Serializable {
    private static final long serialVersionUID = 2595061105075937307L;
    private String username;
    private String password;

    public String getUsername() {
        return username;
    }

    public User setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public User setPassword(String password) {
        this.password = password;
        return this;
    }

    @Override
    public String toString() {
        return "User{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
