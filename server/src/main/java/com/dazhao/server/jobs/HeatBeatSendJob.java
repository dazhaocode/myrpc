package com.dazhao.server.jobs;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * @author dazhao
 * @since 2020/11/25 8:53 下午
 */
public class HeatBeatSendJob {


    /**
     * 定时向组册中心发送心跳信息
     */
    public static void sendOInfo(final List<String> registryHost, final String serverIp, final Integer serverPort){
        Timer timer=new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                for (String host : registryHost) {
                    System.out.println("定时发送心跳 ");
                    String ip=host.split(":")[0];
                    Integer port=Integer.valueOf(host.split(":")[1]);
                    try {
                        Socket socket = new Socket(ip, port);
                        ObjectOutputStream outputStream = new ObjectOutputStream(socket.getOutputStream());
                        ObjectInputStream inputStream = new ObjectInputStream(socket.getInputStream());
                        outputStream.writeUTF("heartbeat");
                        outputStream.writeUTF(serverIp);
                        outputStream.writeInt(serverPort);
                        //关闭
                        outputStream.flush();
                        socket.shutdownOutput();
                        System.out.println(inputStream.readUTF());
                        socket.shutdownInput();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        },0,1000);
    }
}
