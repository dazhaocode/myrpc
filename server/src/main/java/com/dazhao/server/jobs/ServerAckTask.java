package com.dazhao.server.jobs;

import com.dazhao.server.builder.ConfigBuilder;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.List;
import java.util.Map;

/**
 * @author dazhao
 * @since 2020/11/28 9:49 下午
 */
public class ServerAckTask {
    /**
     * 服务ACK
     * @param taskNumber
     * @param ack
     */
    public static void sendInfo(String taskNumber, Integer ack){
        new  Thread(new Runnable() {
            @Override
            public void run() {
                String registry = ConfigBuilder.getRegistryHostList().get(0);
                String host = registry.split(":")[0];
                Integer port = Integer.valueOf(registry.split(":")[1]);
                try {
                    Socket socket = new Socket(host, port);
                    ObjectOutputStream outputStream = new ObjectOutputStream(socket.getOutputStream());
                    ObjectInputStream inputStream = new ObjectInputStream(socket.getInputStream());
                    outputStream.writeUTF("taskACK");
                    outputStream.writeUTF(taskNumber);
                    outputStream.writeUTF(String.valueOf(ack));
                    //关闭
                    outputStream.flush();
                    socket.shutdownOutput();
                    System.out.println(inputStream.readUTF());
                    socket.shutdownInput();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
}
