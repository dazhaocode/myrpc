package com.dazhao.server.jobs;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.List;
import java.util.Map;

/**
 * @author dazhao
 * @since 2020/11/25 9:38 下午
 * 服务注册任务
 */
public class ServerRegistryJob {

    /**
     * 组册服务
     * @param serverList
     * @param host
     * @param port
     */
    public static void sendInfo(final List<Map<String,String>> serverList, final String host, final Integer port, final String serverIp, final Integer serverPort){
        new  Thread(new Runnable() {
            @Override
            public void run() {
                    try {
                        Socket socket = new Socket(host, port);
                        ObjectOutputStream outputStream = new ObjectOutputStream(socket.getOutputStream());
                        ObjectInputStream inputStream = new ObjectInputStream(socket.getInputStream());
                        outputStream.writeUTF("registryServer");
                        outputStream.writeUTF(serverIp);
                        outputStream.writeInt(serverPort);
                        ObjectMapper objectMapper=new ObjectMapper();
                        String json = objectMapper.writeValueAsString(serverList);
                        System.out.println(json);
                        outputStream.writeUTF(json);
                        //关闭
                        outputStream.flush();
                        socket.shutdownOutput();
                        System.out.println(inputStream.readUTF());
                        socket.shutdownInput();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
        }).start();
    }
}
