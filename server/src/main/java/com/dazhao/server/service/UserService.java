package com.dazhao.server.service;

import com.dazhao.server.entity.User;

import java.util.List;

/**
 * @author dazhao
 * @since 2020/11/25 9:01 下午
 */
public interface UserService {
    List<User> queryUserByParam(User user);
    int addUserByParam(User user);
    int updateUserByParam(User user);
    int deleteUserByParam(User user);
}
