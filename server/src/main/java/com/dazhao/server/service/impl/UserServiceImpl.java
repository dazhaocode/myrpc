package com.dazhao.server.service.impl;

import com.dazhao.server.annotation.Service;
import com.dazhao.server.entity.User;
import com.dazhao.server.service.UserService;

import java.util.Arrays;
import java.util.List;

/**
 * @author dazhao
 * @since 2020/11/25 9:02 下午
 */
public class UserServiceImpl implements UserService {

    @Service
    @Override
    public List<User> queryUserByParam(User user) {
        return Arrays.asList(new User().setUsername("dazhao1").setPassword("dazhao1"),
                new User().setUsername("dazhao2").setPassword("dazhao2"));
    }

    @Service
    @Override
    public int addUserByParam(User user) {
        return 0;
    }

    @Service
    @Override
    public int updateUserByParam(User user) {
        return 0;
    }

    @Service
    @Override
    public int deleteUserByParam(User user) {
        return 0;
    }
}
