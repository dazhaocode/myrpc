package com.dazhao.server.tcp;

import com.dazhao.server.builder.ConfigBuilder;
import com.dazhao.server.jobs.HeatBeatSendJob;
import com.dazhao.server.jobs.ServerRegistryJob;
import java.net.ServerSocket;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 通讯服务处理
 * @Author dazhao
 * @Date 2020/11/14 1:22 下午
 */
public class TCPServer {
    //单例
    private static TCPServer tcpServer=null;
    private  Integer port;
    private  Integer maxActivity;
    public  List<Map<String,String>> serverList;
    public  List<String> registryHostList;
    private TCPServer(){
        this.port= ConfigBuilder.getPort();
        this.maxActivity=ConfigBuilder.getMaxActivity();
        this.serverList=ConfigBuilder.getList();
        this.registryHostList= ConfigBuilder.getRegistryHostList();
    }
    public synchronized  static TCPServer newInstance() {
        if (tcpServer==null) {
            tcpServer=new TCPServer();
        }
        return tcpServer;
    }

    /**
     * 初始化启动服务
     */
    public  void initAndStart(){
        try {
            System.out.println("生产者服务启动中....");
            //不关闭服务 一直提供服务
            ServerSocket serverSocket=new ServerSocket(port);
            //根据maxActivity 创建线程池
            ExecutorService executorService= Executors.newFixedThreadPool(this.maxActivity);
            //创建TcpServlet 所有线程公用一个此TcpServlet
            TcpServlet tcpServlet = new TcpServlet();
            System.out.println("生产者服务已经启动！");
            System.out.println("开始注册服务...");
            ServerRegistryJob.sendInfo(this.serverList,"127.0.0.1",9997,"127.0.0.1",this.port);
            System.out.println("生产者服务心跳检测已经启动！");
            HeatBeatSendJob.sendOInfo(this.registryHostList,"127.0.0.1",this.port);

            while (true){
                executorService.execute(new TcpThread(
                        serverSocket.accept(),
                        tcpServlet
                        ));
            }
        } catch (Exception e) {
           e.printStackTrace();
        }
    }
}
