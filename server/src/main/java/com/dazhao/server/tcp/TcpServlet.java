package com.dazhao.server.tcp;


import com.dazhao.server.entity.User;
import com.dazhao.server.jobs.ServerAckTask;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.Socket;
import java.util.Arrays;
import java.util.Map;

/**
 * @author  dazhao
 * @since 2020/11/17 10:47 下午
 * 服务执行
 */
public class TcpServlet {
    /**
     * 生产服务
     * @param outputStream
     * @param inputStream
     * @param socket
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public void sendRequest(ObjectOutputStream outputStream,
                                ObjectInputStream inputStream,
                                    Socket socket) throws IOException, ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InstantiationException, InvocationTargetException {

        String taskNumber = inputStream.readUTF();
        System.out.println(taskNumber);
        Map<String,String> serverMap = (Map<String, String>) inputStream.readObject();
        System.out.println(Arrays.toString(serverMap.keySet().toArray()));
        Class<?>[] paramTypes  = (Class<?>[]) inputStream.readObject();
        System.out.println(Arrays.toString(paramTypes));
        Object[] args = (Object[]) inputStream.readObject();
        System.out.println(Arrays.toString(args));
        //1 向注册中心ack确认获取了任务 异步执行
        // 这里是直接提交 状态码 而不是累加
        // 为了保证的状态码的安全性 可以使用消息队列
        ServerAckTask.sendInfo(taskNumber,2);
        //2 提供服务
        // 生产者通过反射 动态调用接口对应的实现类
        String serverImpl = serverMap.get("serverImpl");
        Class<?> implClass = Class.forName(serverImpl);
        String serverMethod = serverMap.get("serverMethodName");
        Method implClassMethod = implClass.getMethod(serverMethod, paramTypes);
        Object obj = implClassMethod.invoke(implClass.getDeclaredConstructor().newInstance(), args);
        //3 向注册中心注销服务 异步执行
        ServerAckTask.sendInfo(taskNumber,3);
        socket.shutdownInput();
        outputStream.writeUTF("200");
        outputStream.writeObject(obj);
    }

}
