package com.dazhao.server.tcp;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Arrays;

/**
 * @Author dazhao
 * @Date 2020/11/17 10:19 下午
 */
public class TcpThread implements Runnable {
    private final Socket socket;
    private final TcpServlet tcpServlet;
    public TcpThread(final Socket socket,
                                final TcpServlet tcpServlet){
        this.socket=socket;
        this.tcpServlet=tcpServlet;
    }
    @Override
    public void run() {
        //设计协议 用户 密码 行为 参数
        //返回结果说明 200 OK 400 notfound 500 501 account incorrect 502 params incorrect
        ObjectInputStream inputStream=null;
        ObjectOutputStream outputStream=null;
        try {
            inputStream = new ObjectInputStream(this.socket.getInputStream());
            outputStream = new ObjectOutputStream(this.socket.getOutputStream());
            //读取操作类型
            String operation = inputStream.readUTF();
            switch (operation) {
                //分发服务
                case "sendRequest":
                    this.tcpServlet.sendRequest(outputStream,inputStream,socket);
                    break;
            }

        } catch (Exception e) {
            if (outputStream!=null) {
                try {
                    outputStream.writeUTF("500");
//                    //写出异常
//                    outputStream.writeObject(e);
                  e.printStackTrace();
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                }
            }

        }finally {
//            if (inputStream!=null) {
//                try {
//                    inputStream.close();
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
            if (outputStream!=null) {
                try {
                    outputStream.flush();
                    //已经shutdownOutput 可以不关闭
                    this.socket.shutdownOutput();
//                    outputStream.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

    }
}
